﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Xml;

namespace Fractal
{
    public partial class Form1 : Form
    {
        
        

        private const int MAX = 256;      // max iterations
        private const double SX = -2.025; // start value real
        private const double SY = -1.125; // start value imaginary
        private const double EX = 0.6;    // end value real
        private const double EY = 1.125;  // end value imaginary
        private static int x1, y1, xs, ys, xe, ye;
        private static double xstart, ystart, xende, yende, xzoom, yzoom;
        private static Boolean action, rectangle;
        private static float xy;
        private int j;

        private bool mouseDown = false;
        //private Image picture;
        private System.Drawing.Bitmap myBitmap;

        private Graphics g1;
        private Cursor c1, c2;
        //private HSBColor hsb;
        public Form1()
        {

            InitializeComponent();
            // hsb = new HSBColor();
            c1 = Cursors.WaitCursor;
            c2 = Cursors.Cross;
            x1 = pictureBox1.Width;
            y1 = pictureBox1.Height;
            xy = (float)x1 / (float)y1;
            myBitmap = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            g1 = Graphics.FromImage(myBitmap);
            Start();
        }
        public void Start()
        {
            action = false;
            rectangle = false;
            String exists = "state.xml";
            if (File.Exists(exists))
            {
                try
                {
                    XmlDocument state = new XmlDocument();
                    state.Load("state.xml");
                    foreach (XmlNode node in state)
                    {
                        xstart = Convert.ToDouble(node["xstart"]?.InnerText);
                        ystart = Convert.ToDouble(node["ystart"]?.InnerText);
                        xzoom = Convert.ToDouble(node["xzoom"]?.InnerText);
                        yzoom = Convert.ToDouble(node["yzoom"]?.InnerText);
                        j = Convert.ToInt32(node["j"]?.InnerText);
                    }
                    mandelbrot();
                    Refresh();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

            }
            else
            {
                initvalues();
                xzoom = (xende - xstart) / (double)x1;
                yzoom = (yende - ystart) / (double)y1;
                mandelbrot();
                Refresh();
            }
        }
        private void initvalues() // reset start values
        {
            xstart = SX;
            ystart = SY;
            xende = EX;
            yende = EY;
            if ((float)((xende - xstart) / (yende - ystart)) != xy)
                xstart = xende - (yende - ystart) * (double)xy;
        }

        private void mandelbrot() // calculate all points
        {
            int x, y;
            float h, b, alt = 0.0f;
            action = false;
            Color color = Color.White;
            Pen pen = new Pen(color);
            //setCursor(c1);
            pictureBox1.Cursor = c2;


            //showStatus("Mandelbrot-Set will be produced - please wait...");
            for (x = 0; x < x1; x += 2)
            {
                for (y = 0; y < y1; y++)
                {
                    h = pointcolour(xstart + xzoom * (double)x, ystart + yzoom * (double)y);
                    if (h != alt)
                    {
                        b = 1.0f - h * h; // brightnes

                        ///djm added
                        color = HSBColor.FromHSB(new HSBColor(h * 255, 0.8f * 255, b * 255));

                        // g1.setColor(col);
                        //djm end
                        //djm added to convert to RGB from HSB
                        // g1.setColor(Color.getHSBColor(h, 0.8f, b));
                        //djm test
                        //  Color col = Color.getHSBColor(h, 0.8f, b);
                        //  int red = col.getRed();
                        // int green = col.getGreen();
                        //  int blue = col.getBlue();
                        //djm 
                        pen = new Pen(color);
                        alt = h;



                    }
                    g1.DrawLine(pen, new Point(x, y), new Point(x + 1, y));
                }

                //showStatus("Mandelbrot-Set ready - please select zoom area with pressed mouse.");
                //setCursor(c2);
                
            }
            pictureBox1.Image = myBitmap;
            //pictureBox1.Cursor = c2;
            action = true;
        }


        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            if (action)
            {
                mouseDown = true;
                xs = e.X;
                ys = e.Y;
            }
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            j = 100;
            mandelbrot();
            Refresh();
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            j = 200;
            mandelbrot();
            Refresh();
        }

        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {
            j = 50;
            mandelbrot();
            Refresh();
        }

        private void toolStripMenuItem5_Click(object sender, EventArgs e)
        {
            j = 0;
            mandelbrot();
            Refresh();
        }

        private void ColorCycleTimer_Tick(object sender, EventArgs e)
        {
            mandelbrot();
            Refresh();
            if (j <= 240) {
                j++;
            }
        }


        private void startToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ColorCycleTimer.Start();
            ColorCycleTimer.Interval = 10;
        }

        private void stopToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ColorCycleTimer.Stop();
           
        }

        private void toolStripMenuItem6_Click(object sender, EventArgs e)
        {
            j = 25;
            mandelbrot();
            Refresh();

        }

        private void toolStripMenuItem7_Click(object sender, EventArgs e)
        {
            j = 125;
            mandelbrot();
            Refresh();

        }

        private void toolStripMenuItem8_Click(object sender, EventArgs e)
        {
            j = 150;
            mandelbrot();
            Refresh();

        }

        private void toolStripMenuItem9_Click(object sender, EventArgs e)
        {
            j = 230;
            mandelbrot();
            Refresh();

        }

        

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (action && mouseDown)
            {
                xe = e.X;
                ye = e.Y;
                rectangle = true;
                pictureBox1.Refresh();
            }
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            int z, w;
            if (action)
            {
                xe = e.X;
                ye = e.Y;
                if (xs > xe)
                {
                    z = xs;
                    xs = xe;
                    xe = z;
                }
                if (ys > ye)
                {
                    z = ys;
                    ys = ye;
                    ye = z;
                }
                w = (xe - xs);
                z = (ye - ys);
                if ((w < 2) && (z < 2))
                    initvalues();
                else
                {
                    if (((float)w > (float)z * xy))
                        ye = (int)((float)ys + (float)w / xy);
                    else
                        xe = (int)((float)xs + (float)z * xy);
                    xende = xstart + xzoom * (double)xe;
                    yende = ystart + yzoom * (double)ye;
                    xstart += xzoom * (double)xs;
                    ystart += yzoom * (double)ys;
                }
                xzoom = (xende - xstart) / (double)x1;
                yzoom = (yende - ystart) / (double)y1;
                mandelbrot();
                rectangle = false;
                pictureBox1.Refresh();
                mouseDown = false;
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {

            //show message box to confirm user request
            //MessageBox.Show(<message>,<title>,<buttons>)
            if (MessageBox.Show("Are you sure, You want to close the application?", "Exit", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                Application.Exit();
            }
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //opens 'About' form
            //About about = new About();
            //about.ShowDialog();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveImage = new SaveFileDialog();
            saveImage.Filter = "PNG Image(*.png) | *.png | JPG Image(*.jpg) | *.jpg | BMP Image(*.bmp) | *.bmp";
            if (saveImage.ShowDialog() == DialogResult.OK)
            {
                myBitmap.Save(saveImage.FileName);
            }
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {

            pictureBox1.Image = null;
            Start();
        }

        private void stateSaveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                XmlWriter writer = XmlWriter.Create("state.xml");
                writer.WriteStartDocument();
                writer.WriteStartElement("states");
                writer.WriteElementString("xstart",xstart.ToString());
                writer.WriteElementString("ystart",ystart.ToString());
                writer.WriteElementString("xzoom",xzoom.ToString());
                writer.WriteElementString("yzoom",yzoom.ToString());
                writer.WriteElementString("j", j.ToString());
                writer.WriteEndElement();
                writer.WriteEndDocument();
                writer.Flush();
                writer.Close();
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
        }


       
        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            Image temp = Image.FromHbitmap(myBitmap.GetHbitmap());
            Graphics g = Graphics.FromImage(temp);
            if (rectangle)
            {
                Pen pen = new Pen(Color.White);
                Rectangle rect;
                if (xs < xe)
                {
                    if (ys < ye)
                    {
                        rect = new Rectangle(xs, ys, (xe - xs), (ye - ys));
                    }
                    else
                    {
                        rect = new Rectangle(xs, ye, (xe - xs), (ys - ye));
                    }
                }
                else
                {
                    if (ys < ye)
                    {
                        rect = new Rectangle(xe, ys, (xs - xe), (ye - ys));
                    }
                    else
                    {
                        rect = new Rectangle(xe, ye, (xs - xe), (ys - ye));
                    }
                }
                g.DrawRectangle(pen, rect);
                pictureBox1.Image = temp;
            }
        }
   
        

        /*public void Destroy() // delete all instances 
        {
            if (finished)
            {
                // removeMouseListener(this);
                // removeMouseMotionListener(this);
                //picture = null;
                myBitmap = null;
                g1 = null;
                c1 = null;
                c2 = null;
                GC.Collect(); // garbage collection
            }
        }*/
       




        /*public void stop()
        {
        }*/

        /*public void paint(Graphics g)
        {
            update(g);
        }*/
        /*public void update(Graphics g)
        {
               // g.drawImage(picture, 0, 0, this);
            g.DrawImage(myBitmap, 0, 0, x1,y1);
            if (rectangle)
                {
                
                Pen pen = new Pen(Color.White);
                Rectangle rect;

                // g.setColor(Color.white);
                if (xs < xe)
                {
                    if (ys < ye)
                    {
                        rect = new Rectangle(xs, ys, (xe - xs), (ye - ys));
                    }

                    else
                    {
                        rect = new Rectangle(xs, ye, (xe - xs), (ys - ye));
                    }
                }
                else
                {
                    if (ys < ye)
                    {
                        rect = new Rectangle(xe, ys, (xs - xe), (ye - ys));
                    }
                    else
                    {
                        rect = new Rectangle(xe, ye, (xs - xe), (ys - ye));
                    }
                }

                g.DrawRectangle(pen, rect);

            }
        }*/
       
        private float pointcolour(double xwert, double ywert) // color value from 0.0 to 1.0 by iterations
        {
            double r = 0.0, i = 0.0, m = 0.0;
            int j1 = j;
            while ((j1 < MAX) && (m < 4.0))
            {
                j1++;
                m = r * r - i * i;
                i = 2.0 * r * i + ywert;
                r = m + xwert;
            }
            return (float)j1 / (float)MAX;
        }
        
        /* public void mousePressed(MouseEvent e)
         {
             e.consume();
             if (action)
             {
                 xs = e.getX();
                 ys = e.getY();
             }
         }
         */


       


        /*public void mouseReleased(MouseEvent e)
        {
            int z, w;

            e.consume();
            if (action)
            {
                xe = e.getX();
                ye = e.getY();
                if (xs > xe)
                {
                    z = xs;
                    xs = xe;
                    xe = z;
                }
                if (ys > ye)
                {
                    z = ys;
                    ys = ye;
                    ye = z;
                }
                w = (xe - xs);
                z = (ye - ys);
                if ((w < 2) && (z < 2)) initvalues();
                else
                {
                    if (((float)w > (float)z * xy)) ye = (int)((float)ys + (float)w / xy);
                    else xe = (int)((float)xs + (float)z * xy);
                    xende = xstart + xzoom * (double)xe;
                    yende = ystart + yzoom * (double)ye;
                    xstart += xzoom * (double)xs;
                    ystart += yzoom * (double)ys;
                }
                xzoom = (xende - xstart) / (double)x1;
                yzoom = (yende - ystart) / (double)y1;
                mandelbrot();
                rectangle = false;
                repaint();
            }
        }
        */
        private void Form1_MouseUp_1(object sender, MouseEventArgs e)
        { 
            int z, w;

            if (action)
            {
                xe = e.X;
                ye = e.Y;
                if (xs > xe)
                {
                    z = xs;
                    xs = xe;
                    xe = z;
                }
                if (ys > ye)
                {
                    z = ys;
                    ys = ye;
                    ye = z;
                }
                w = (xe - xs);
                z = (ye - ys);
                if ((w < 2) && (z < 2)) initvalues();
                else
                {
                    if (((float)w > (float)z * xy)) ye = (int)((float)ys + (float)w / xy);
                    else xe = (int)((float)xs + (float)z * xy);
                    xende = xstart + xzoom * (double)xe;
                    yende = ystart + yzoom * (double)ye;
                    xstart += xzoom * (double)xs;
                    ystart += yzoom * (double)ys;
                }
                xzoom = (xende - xstart) / (double)x1;
                yzoom = (yende - ystart) / (double)y1;
                mandelbrot();
                rectangle = false;
                pictureBox1.Refresh();
                //repaint();
                mouseDown = false;

            }
        }

        /*public void mouseEntered(MouseEvent e)
        {
        }*/

        /* public void mouseExited(MouseEvent e)
         {
         }*/

        /* public void mouseClicked(MouseEvent e)
         {
         }*/

         /*public void mouseDragged(MouseEvent e)
         {
             e.consume();
             if (action)
             {
                 xe = e.getX();
                 ye = e.getY();
                 rectangle = true;
                 repaint();
             }
         }*/
          private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            if (action && mouseDown)
            {
                xe = e.X;
                ye = e.Y;
                rectangle = true;
                pictureBox1.Refresh();

            }
        }

        /* public void mouseMoved(MouseEvent e)
         {
         }
         */

        /* public String getAppletInfo()
         {
             return "fractal.class - Mandelbrot Set a Java Applet by Eckhard Roessel 2000-2001";
         }
         */
        public struct HSBColor
        {
            float h;
            float s;
            float b;
            int a;
            public HSBColor(float h, float s, float b)
            {
                this.a = 0xff;
                this.h = Math.Min(Math.Max(h, 0), 255);
                this.s = Math.Min(Math.Max(s, 0), 255);
                this.b = Math.Min(Math.Max(b, 0), 255);
            }
            public HSBColor(int a, float h, float s, float b)
            {
                this.a = a;
                this.h = Math.Min(Math.Max(h, 0), 255);
                this.s = Math.Min(Math.Max(s, 0), 255);
                this.b = Math.Min(Math.Max(b, 0), 255);
            }
            public float H
            {
                get { return h; }
            }
            public float S
            {
                get { return s; }
            }
            public float B
            {
                get { return b; }
            }
            public int A
            {
                get { return a; }
            }
            public Color Color
            {
                get
                {
                    return FromHSB(this);
                }
            }
            public static Color FromHSB(HSBColor hsbColor)
            {
                float r = hsbColor.b;
                float g = hsbColor.b;
                float b = hsbColor.b;
                if (hsbColor.s != 0)
                {
                    float max = hsbColor.b;
                    float dif = hsbColor.b * hsbColor.s / 255f;
                    float min = hsbColor.b - dif;
                    float h = hsbColor.h * 360f / 255f;
                    if (h < 60f)
                    {
                        r = max;
                        g = h * dif / 60f + min;
                        b = min;
                    }
                    else if (h < 120f)
                    {
                        r = -(h - 120f) * dif / 60f + min;
                        g = max;
                        b = min;
                    }
                    else if (h < 180f)
                    {
                        r = min;
                        g = max;
                        b = (h - 120f) * dif / 60f + min;
                    }
                    else if (h < 240f)
                    {
                        r = min;
                        g = -(h - 240f) * dif / 60f + min;
                        b = max;
                    }
                    else if (h < 300f)
                    {
                        r = (h - 240f) * dif / 60f + min;
                        g = min;
                        b = max;
                    }
                    else if (h <= 360f)
                    {
                        r = max;
                        g = min;
                        b = -(h - 360f) * dif / 60 + min;
                    }
                    else
                    {
                        r = 0;
                        g = 0;
                        b = 0;
                    }
                }
                return Color.FromArgb
                    (
                        hsbColor.a,
                        (int)Math.Round(Math.Min(Math.Max(r, 0), 255)),
                        (int)Math.Round(Math.Min(Math.Max(g, 0), 255)),
                        (int)Math.Round(Math.Min(Math.Max(b, 0), 255))
                        );
            }
        }
    }
}
